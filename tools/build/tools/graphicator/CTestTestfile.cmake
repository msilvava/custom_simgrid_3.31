# CMake generated Testfile for 
# Source directory: /home/migvasc/migvasc/simgrid/tools/graphicator
# Build directory: /home/migvasc/migvasc/simgrid/build/tools/graphicator
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(graphicator "/usr/bin/python3.8" "/home/migvasc/migvasc/simgrid/build/bin/tesh" "--ignore-jenkins" "--setenv" "srcdir=/home/migvasc/migvasc/simgrid" "--setenv" "bindir=/home/migvasc/migvasc/simgrid/build/bin" "--cd" "/home/migvasc/migvasc/simgrid/build/tools/graphicator" "/home/migvasc/migvasc/simgrid/tools/graphicator/graphicator.tesh")
set_tests_properties(graphicator PROPERTIES  _BACKTRACE_TRIPLES "/home/migvasc/migvasc/simgrid/tools/cmake/Tests.cmake;49;ADD_TEST;/home/migvasc/migvasc/simgrid/tools/graphicator/CMakeLists.txt;5;ADD_TESH;/home/migvasc/migvasc/simgrid/tools/graphicator/CMakeLists.txt;0;")
