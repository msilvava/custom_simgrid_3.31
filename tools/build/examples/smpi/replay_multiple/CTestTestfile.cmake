# CMake generated Testfile for 
# Source directory: /home/migvasc/migvasc/simgrid/examples/smpi/replay_multiple
# Build directory: /home/migvasc/migvasc/simgrid/build/examples/smpi/replay_multiple
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(smpi-replay-multiple "/usr/bin/python3.8" "/home/migvasc/migvasc/simgrid/build/bin/tesh" "--ignore-jenkins" "--setenv" "srcdir=/home/migvasc/migvasc/simgrid/examples/smpi/replay_multiple" "--setenv" "bindir=/home/migvasc/migvasc/simgrid/build/examples/smpi/replay_multiple" "--cd" "/home/migvasc/migvasc/simgrid/build/examples/smpi/replay_multiple" "/home/migvasc/migvasc/simgrid/examples/smpi/replay_multiple/replay_multiple.tesh")
set_tests_properties(smpi-replay-multiple PROPERTIES  _BACKTRACE_TRIPLES "/home/migvasc/migvasc/simgrid/tools/cmake/Tests.cmake;49;ADD_TEST;/home/migvasc/migvasc/simgrid/examples/smpi/replay_multiple/CMakeLists.txt;6;ADD_TESH;/home/migvasc/migvasc/simgrid/examples/smpi/replay_multiple/CMakeLists.txt;0;")
