# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.16

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/migvasc/migvasc/simgrid

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/migvasc/migvasc/simgrid/build

# Include any dependencies generated for this target.
include examples/smpi/CMakeFiles/smpi_gemm.dir/depend.make

# Include the progress variables for this target.
include examples/smpi/CMakeFiles/smpi_gemm.dir/progress.make

# Include the compile flags for this target's objects.
include examples/smpi/CMakeFiles/smpi_gemm.dir/flags.make

examples/smpi/CMakeFiles/smpi_gemm.dir/gemm/gemm.c.o: examples/smpi/CMakeFiles/smpi_gemm.dir/flags.make
examples/smpi/CMakeFiles/smpi_gemm.dir/gemm/gemm.c.o: ../examples/smpi/gemm/gemm.c
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/migvasc/migvasc/simgrid/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building C object examples/smpi/CMakeFiles/smpi_gemm.dir/gemm/gemm.c.o"
	cd /home/migvasc/migvasc/simgrid/build/examples/smpi && /home/migvasc/migvasc/simgrid/build/smpi_script/bin/smpicc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -o CMakeFiles/smpi_gemm.dir/gemm/gemm.c.o   -c /home/migvasc/migvasc/simgrid/examples/smpi/gemm/gemm.c

examples/smpi/CMakeFiles/smpi_gemm.dir/gemm/gemm.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/smpi_gemm.dir/gemm/gemm.c.i"
	cd /home/migvasc/migvasc/simgrid/build/examples/smpi && /home/migvasc/migvasc/simgrid/build/smpi_script/bin/smpicc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -E /home/migvasc/migvasc/simgrid/examples/smpi/gemm/gemm.c > CMakeFiles/smpi_gemm.dir/gemm/gemm.c.i

examples/smpi/CMakeFiles/smpi_gemm.dir/gemm/gemm.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/smpi_gemm.dir/gemm/gemm.c.s"
	cd /home/migvasc/migvasc/simgrid/build/examples/smpi && /home/migvasc/migvasc/simgrid/build/smpi_script/bin/smpicc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -S /home/migvasc/migvasc/simgrid/examples/smpi/gemm/gemm.c -o CMakeFiles/smpi_gemm.dir/gemm/gemm.c.s

# Object files for target smpi_gemm
smpi_gemm_OBJECTS = \
"CMakeFiles/smpi_gemm.dir/gemm/gemm.c.o"

# External object files for target smpi_gemm
smpi_gemm_EXTERNAL_OBJECTS =

examples/smpi/gemm/smpi_gemm: examples/smpi/CMakeFiles/smpi_gemm.dir/gemm/gemm.c.o
examples/smpi/gemm/smpi_gemm: examples/smpi/CMakeFiles/smpi_gemm.dir/build.make
examples/smpi/gemm/smpi_gemm: lib/libsimgrid.so.3.31.1
examples/smpi/gemm/smpi_gemm: /usr/lib/x86_64-linux-gnu/libboost_context.so.1.71.0
examples/smpi/gemm/smpi_gemm: /usr/lib/x86_64-linux-gnu/libboost_stacktrace_backtrace.so.1.71.0
examples/smpi/gemm/smpi_gemm: examples/smpi/CMakeFiles/smpi_gemm.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/home/migvasc/migvasc/simgrid/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Linking C executable gemm/smpi_gemm"
	cd /home/migvasc/migvasc/simgrid/build/examples/smpi && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/smpi_gemm.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
examples/smpi/CMakeFiles/smpi_gemm.dir/build: examples/smpi/gemm/smpi_gemm

.PHONY : examples/smpi/CMakeFiles/smpi_gemm.dir/build

examples/smpi/CMakeFiles/smpi_gemm.dir/clean:
	cd /home/migvasc/migvasc/simgrid/build/examples/smpi && $(CMAKE_COMMAND) -P CMakeFiles/smpi_gemm.dir/cmake_clean.cmake
.PHONY : examples/smpi/CMakeFiles/smpi_gemm.dir/clean

examples/smpi/CMakeFiles/smpi_gemm.dir/depend:
	cd /home/migvasc/migvasc/simgrid/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/migvasc/migvasc/simgrid /home/migvasc/migvasc/simgrid/examples/smpi /home/migvasc/migvasc/simgrid/build /home/migvasc/migvasc/simgrid/build/examples/smpi /home/migvasc/migvasc/simgrid/build/examples/smpi/CMakeFiles/smpi_gemm.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : examples/smpi/CMakeFiles/smpi_gemm.dir/depend

