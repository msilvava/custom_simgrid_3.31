# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/migvasc/migvasc/simgrid/examples/c/io-file-remote/io-file-remote.c" "/home/migvasc/migvasc/simgrid/build/examples/c/CMakeFiles/c-io-file-remote.dir/io-file-remote/io-file-remote.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "BOOST_ALL_NO_LIB"
  "BOOST_CONTEXT_DYN_LINK"
  "BOOST_STACKTRACE_BACKTRACE_DYN_LINK"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "include"
  "../include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/migvasc/migvasc/simgrid/build/CMakeFiles/simgrid.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "/home/migvasc/migvasc/simgrid/build/include/smpi")
