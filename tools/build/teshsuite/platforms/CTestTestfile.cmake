# CMake generated Testfile for 
# Source directory: /home/migvasc/migvasc/simgrid/teshsuite/platforms
# Build directory: /home/migvasc/migvasc/simgrid/build/teshsuite/platforms
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(tesh-platform-flatifier "/usr/bin/python3.8" "/home/migvasc/migvasc/simgrid/build/bin/tesh" "--ignore-jenkins" "--setenv" "bindir=/home/migvasc/migvasc/simgrid/build/teshsuite/platforms" "--setenv" "srcdir=/home/migvasc/migvasc/simgrid" "--cd" "/home/migvasc/migvasc/simgrid/teshsuite/platforms" "flatifier.tesh")
set_tests_properties(tesh-platform-flatifier PROPERTIES  _BACKTRACE_TRIPLES "/home/migvasc/migvasc/simgrid/tools/cmake/Tests.cmake;49;ADD_TEST;/home/migvasc/migvasc/simgrid/teshsuite/platforms/CMakeLists.txt;44;ADD_TESH;/home/migvasc/migvasc/simgrid/teshsuite/platforms/CMakeLists.txt;0;")
add_test(tesh-parser-bogus-symmetric "/usr/bin/python3.8" "/home/migvasc/migvasc/simgrid/build/bin/tesh" "--ignore-jenkins" "--setenv" "bindir=/home/migvasc/migvasc/simgrid/build/teshsuite/platforms" "--cd" "/home/migvasc/migvasc/simgrid/teshsuite/platforms" "/home/migvasc/migvasc/simgrid/teshsuite/platforms/bogus_two_hosts_asymetric.tesh")
set_tests_properties(tesh-parser-bogus-symmetric PROPERTIES  _BACKTRACE_TRIPLES "/home/migvasc/migvasc/simgrid/tools/cmake/Tests.cmake;49;ADD_TEST;/home/migvasc/migvasc/simgrid/teshsuite/platforms/CMakeLists.txt;48;ADD_TESH;/home/migvasc/migvasc/simgrid/teshsuite/platforms/CMakeLists.txt;0;")
add_test(tesh-parser-bogus-missing-gw "/usr/bin/python3.8" "/home/migvasc/migvasc/simgrid/build/bin/tesh" "--ignore-jenkins" "--setenv" "bindir=/home/migvasc/migvasc/simgrid/build/teshsuite/platforms" "--cd" "/home/migvasc/migvasc/simgrid/teshsuite/platforms" "/home/migvasc/migvasc/simgrid/teshsuite/platforms/bogus_missing_gateway.tesh")
set_tests_properties(tesh-parser-bogus-missing-gw PROPERTIES  _BACKTRACE_TRIPLES "/home/migvasc/migvasc/simgrid/tools/cmake/Tests.cmake;49;ADD_TEST;/home/migvasc/migvasc/simgrid/teshsuite/platforms/CMakeLists.txt;49;ADD_TESH;/home/migvasc/migvasc/simgrid/teshsuite/platforms/CMakeLists.txt;0;")
