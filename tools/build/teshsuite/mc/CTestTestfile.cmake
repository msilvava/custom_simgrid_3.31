# CMake generated Testfile for 
# Source directory: /home/migvasc/migvasc/simgrid/teshsuite/mc
# Build directory: /home/migvasc/migvasc/simgrid/build/teshsuite/mc
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(mc-random-bug-replay "/usr/bin/python3.8" "/home/migvasc/migvasc/simgrid/build/bin/tesh" "--ignore-jenkins" "--setenv" "platfdir=/home/migvasc/migvasc/simgrid/examples/platforms" "--setenv" "bindir=/home/migvasc/migvasc/simgrid/build/teshsuite/mc/random-bug" "--cd" "/home/migvasc/migvasc/simgrid/teshsuite/mc/random-bug" "random-bug-replay.tesh")
set_tests_properties(mc-random-bug-replay PROPERTIES  _BACKTRACE_TRIPLES "/home/migvasc/migvasc/simgrid/tools/cmake/Tests.cmake;49;ADD_TEST;/home/migvasc/migvasc/simgrid/teshsuite/mc/CMakeLists.txt;57;ADD_TESH;/home/migvasc/migvasc/simgrid/teshsuite/mc/CMakeLists.txt;0;")
