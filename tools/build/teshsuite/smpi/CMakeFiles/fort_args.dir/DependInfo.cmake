# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "Fortran"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_Fortran
  "/home/migvasc/migvasc/simgrid/teshsuite/smpi/fort_args/fort_args.f90" "/home/migvasc/migvasc/simgrid/build/teshsuite/smpi/CMakeFiles/fort_args.dir/fort_args/fort_args.f90.o"
  )
set(CMAKE_Fortran_COMPILER_ID "GNU")
set(CMAKE_Fortran_SUBMODULE_SEP "@")
set(CMAKE_Fortran_SUBMODULE_EXT ".smod")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_Fortran
  "BOOST_ALL_NO_LIB"
  "BOOST_CONTEXT_DYN_LINK"
  "BOOST_STACKTRACE_BACKTRACE_DYN_LINK"
  )

# The include file search paths:
set(CMAKE_Fortran_TARGET_INCLUDE_PATH
  "../include/smpi"
  "include"
  "../include"
  "/usr/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/migvasc/migvasc/simgrid/build/CMakeFiles/simgrid.dir/DependInfo.cmake"
  "/home/migvasc/migvasc/simgrid/build/CMakeFiles/mpi.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "/home/migvasc/migvasc/simgrid/build/include/smpi")
