# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.16

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/migvasc/migvasc/simgrid

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/migvasc/migvasc/simgrid/build

# Include any dependencies generated for this target.
include teshsuite/smpi/CMakeFiles/io-simple-at.dir/depend.make

# Include the progress variables for this target.
include teshsuite/smpi/CMakeFiles/io-simple-at.dir/progress.make

# Include the compile flags for this target's objects.
include teshsuite/smpi/CMakeFiles/io-simple-at.dir/flags.make

teshsuite/smpi/CMakeFiles/io-simple-at.dir/io-simple-at/io-simple-at.c.o: teshsuite/smpi/CMakeFiles/io-simple-at.dir/flags.make
teshsuite/smpi/CMakeFiles/io-simple-at.dir/io-simple-at/io-simple-at.c.o: ../teshsuite/smpi/io-simple-at/io-simple-at.c
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/migvasc/migvasc/simgrid/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building C object teshsuite/smpi/CMakeFiles/io-simple-at.dir/io-simple-at/io-simple-at.c.o"
	cd /home/migvasc/migvasc/simgrid/build/teshsuite/smpi && /home/migvasc/migvasc/simgrid/build/smpi_script/bin/smpicc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -o CMakeFiles/io-simple-at.dir/io-simple-at/io-simple-at.c.o   -c /home/migvasc/migvasc/simgrid/teshsuite/smpi/io-simple-at/io-simple-at.c

teshsuite/smpi/CMakeFiles/io-simple-at.dir/io-simple-at/io-simple-at.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/io-simple-at.dir/io-simple-at/io-simple-at.c.i"
	cd /home/migvasc/migvasc/simgrid/build/teshsuite/smpi && /home/migvasc/migvasc/simgrid/build/smpi_script/bin/smpicc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -E /home/migvasc/migvasc/simgrid/teshsuite/smpi/io-simple-at/io-simple-at.c > CMakeFiles/io-simple-at.dir/io-simple-at/io-simple-at.c.i

teshsuite/smpi/CMakeFiles/io-simple-at.dir/io-simple-at/io-simple-at.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/io-simple-at.dir/io-simple-at/io-simple-at.c.s"
	cd /home/migvasc/migvasc/simgrid/build/teshsuite/smpi && /home/migvasc/migvasc/simgrid/build/smpi_script/bin/smpicc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -S /home/migvasc/migvasc/simgrid/teshsuite/smpi/io-simple-at/io-simple-at.c -o CMakeFiles/io-simple-at.dir/io-simple-at/io-simple-at.c.s

# Object files for target io-simple-at
io__simple__at_OBJECTS = \
"CMakeFiles/io-simple-at.dir/io-simple-at/io-simple-at.c.o"

# External object files for target io-simple-at
io__simple__at_EXTERNAL_OBJECTS =

teshsuite/smpi/io-simple-at/io-simple-at: teshsuite/smpi/CMakeFiles/io-simple-at.dir/io-simple-at/io-simple-at.c.o
teshsuite/smpi/io-simple-at/io-simple-at: teshsuite/smpi/CMakeFiles/io-simple-at.dir/build.make
teshsuite/smpi/io-simple-at/io-simple-at: lib/libsimgrid.so.3.31.1
teshsuite/smpi/io-simple-at/io-simple-at: /usr/lib/x86_64-linux-gnu/libboost_context.so.1.71.0
teshsuite/smpi/io-simple-at/io-simple-at: /usr/lib/x86_64-linux-gnu/libboost_stacktrace_backtrace.so.1.71.0
teshsuite/smpi/io-simple-at/io-simple-at: teshsuite/smpi/CMakeFiles/io-simple-at.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/home/migvasc/migvasc/simgrid/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Linking C executable io-simple-at/io-simple-at"
	cd /home/migvasc/migvasc/simgrid/build/teshsuite/smpi && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/io-simple-at.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
teshsuite/smpi/CMakeFiles/io-simple-at.dir/build: teshsuite/smpi/io-simple-at/io-simple-at

.PHONY : teshsuite/smpi/CMakeFiles/io-simple-at.dir/build

teshsuite/smpi/CMakeFiles/io-simple-at.dir/clean:
	cd /home/migvasc/migvasc/simgrid/build/teshsuite/smpi && $(CMAKE_COMMAND) -P CMakeFiles/io-simple-at.dir/cmake_clean.cmake
.PHONY : teshsuite/smpi/CMakeFiles/io-simple-at.dir/clean

teshsuite/smpi/CMakeFiles/io-simple-at.dir/depend:
	cd /home/migvasc/migvasc/simgrid/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/migvasc/migvasc/simgrid /home/migvasc/migvasc/simgrid/teshsuite/smpi /home/migvasc/migvasc/simgrid/build /home/migvasc/migvasc/simgrid/build/teshsuite/smpi /home/migvasc/migvasc/simgrid/build/teshsuite/smpi/CMakeFiles/io-simple-at.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : teshsuite/smpi/CMakeFiles/io-simple-at.dir/depend

